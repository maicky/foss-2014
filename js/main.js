
var cuadradosAlto=10;
var cuadradosAncho=10;
var tamCuadrados=32;
var cuadradosAbiertos;
var cuadradosAbiertosTuyos;
var agua=new Image();
var cuadradoRojo=new Image();
var cuadradoVerde=new Image();
var disparoAgua=new Image();
var cuadradoEscaner=new Image();
var cuadradoBomba=new Image();
var imgBomba=new Image();
var imgEscaner=new Image();
var barco=new Image();
var cuadradoSelecX=-1;
var cuadradoSelecY=-1;
var esMiTurno=false;
var colocandoBarcos=true;
var numeroBarcosPorColocarTamanioAct;
var tamanioBarcoColocarAct;
var barcoColocandoHorizontal=true;

var dificultad=1;

var numeroBarcos;

var barcosFaltanColocar;

var numBombas=3;
var numEscaners=2;

var bombaActivada=false;
var escanerActivado=false;
var contraJUgador=false;
var tusPuntos=0;

var cuantasPartidas;
var puntos1;
var puntos2;

var sonidoAcierto=new Audio("sounds/acierto.mp3");
var sonidoFallo=new Audio("sounds/fail.wav");
var sonidoBigExplosion=new Audio("sounds/bomba.wav");
var sonidoEscaner=new Audio("sounds/escaner.wav");
var sonidoWin=new Audio("sounds/win.mp3");
var sonidoLose=new Audio("sounds/lose.wav");



function iniciar(){

    $('#myModal').modal('show');
    if(getQueryVariable("nivel")==-1){
      contraJUgador=true;
    }

    dificultad=getQueryVariable("nivel")-1;
    cuadradosAlto=getQueryVariable("tam");
    cuadradosAncho=getQueryVariable("tam");
    cuantasPartidas=getQueryVariable("numPartidas");
    puntos1=getQueryVariable("puntos1");
    puntos2=getQueryVariable("puntos2");

    document.addEventListener('keydown', function(event) {
      if(event.keyCode==82){
        barcoColocandoHorizontal=!barcoColocandoHorizontal;
        dibujar();
      }
     
    });

    numeroBarcos=new Array();
    numeroBarcos.push(getQueryVariable("barcos1"));
    numeroBarcos.push(getQueryVariable("barcos2"));
    numeroBarcos.push(getQueryVariable("barcos3"));
    numeroBarcos.push(getQueryVariable("barcos4"));

    numBombas=getQueryVariable("bombas");
    numEscaners=getQueryVariable("escaners");
    actualizarArmas();
    barcosFaltanColocar=parseInt(getQueryVariable("barcos1"))+parseInt(getQueryVariable("barcos2"))+parseInt(getQueryVariable("barcos3"))+parseInt(getQueryVariable("barcos4"));
    actualizarLabelColocandoBarcos();

    numeroBarcosPorColocarTamanioAct=numeroBarcos[numeroBarcos.length-1];
    tamanioBarcoColocarAct=numeroBarcos.length;
    console.log(numeroBarcosPorColocarTamanioAct);



    agua.src = "img/agua.png";
    cuadradoVerde.src = "img/cuadradoVerde.png";
    cuadradoRojo.src = "img/cuadradoRojo.png";
    disparoAgua.src = "img/disparoAgua.png";
    cuadradoBomba.src="img/cuadradoBomba.png";
    cuadradoEscaner.src="img/cuadradoEscaner.png";
    imgBomba.src="img/bomba.png";
    imgEscaner.src="img/escaner.png";
    barco.src = "img/barco.png";
    cuadradosAbiertos=new Array();
    for(var i=0;i<cuadradosAncho;i++){
      var linea=new Array();
      for(var j=0;j<cuadradosAlto;j++){
        linea.push(-1);
      }
      cuadradosAbiertos.push(linea);
    }

    cuadradosAbiertosTuyos=new Array();
    for(var i=0;i<cuadradosAncho;i++){
      var linea=new Array();
      for(var j=0;j<cuadradosAlto;j++){
        linea.push(-1);
      }
      cuadradosAbiertosTuyos.push(linea);
    }

    //iniciarTusBarcos();
    if(!contraJUgador){
      iniciarSusBarcos();
    }


    agua.onload = function(){
      dibujar();
    }

    var c = document.getElementById("myCanvas");
    c.addEventListener('mousemove', function(evt) {

        var mousePos = getMousePos(c, evt);
        var message = 'Mouse position: ' + mousePos.x + ',' + mousePos.y;
        //console.log(message);
         if(colocandoBarcos){
          cuadradoSelecX=Math.round(-0.5+(mousePos.x-cuadradosAncho*tamCuadrados-100)/tamCuadrados);
          cuadradoSelecY=Math.round(-0.5+mousePos.y/tamCuadrados);
        }else{
          cuadradoSelecX=Math.round(-0.5+mousePos.x/tamCuadrados);
          cuadradoSelecY=Math.round(-0.5+mousePos.y/tamCuadrados);
        }
       
        dibujar();
      });

    c.addEventListener('click', function(evt) {
        if(esMiTurno){
          var mousePos = getMousePos(c, evt);
          var message = 'Mouse position: ' + mousePos.x + ',' + mousePos.y;
          cuadradoSelecX=Math.round(-0.5+mousePos.x/tamCuadrados);
          cuadradoSelecY=Math.round(-0.5+mousePos.y/tamCuadrados);
          if(cuadradoSelecX>=0&&cuadradoSelecX<cuadradosAncho&&cuadradoSelecY>=0&&cuadradoSelecY<cuadradosAlto){
            disparar(cuadradoSelecX, cuadradoSelecY);
          }
        }else if(colocandoBarcos){
          if(entraBarco(true,tamanioBarcoColocarAct,cuadradoSelecX,cuadradoSelecY,barcoColocandoHorizontal)){
            colocarBarco(true,tamanioBarcoColocarAct,cuadradoSelecX,cuadradoSelecY,barcoColocandoHorizontal);
            avanzarEnColocarBarcos();
          }
        }
      });


    document.getElementById("lBarcosPropios").style.width=cuadradosAncho*tamCuadrados;
    document.getElementById("lBarcosEnemigos").style.width=cuadradosAncho*tamCuadrados;
    document.getElementById("lBarcosEnemigos").style.left=(cuadradosAncho*tamCuadrados)+125;
    document.getElementById("lTurno").style.width=cuadradosAncho*tamCuadrados*2+100;
    document.getElementById("lTurno").style.top=20;
    document.getElementById("lTurnoContrario").style.width=cuadradosAncho*tamCuadrados*2+100;
    document.getElementById("lTurnoContrario").style.top=20;
    document.getElementById("lBarcosEnemigos").style.top=95;
    document.getElementById("lBarcosPropios").style.top=95;

    document.getElementById("avisoColocar").style.width=cuadradosAncho*tamCuadrados*2+100;
    document.getElementById("colocandoActual").style.width=cuadradosAncho*tamCuadrados*2+100;
    document.getElementById("colocandoActual").style.top=95;
    document.getElementById("botonesArmas").style.top=cuadradosAlto*tamCuadrados+200;

}
function disparar(posX,posY){
  if(!bombaActivada&&!escanerActivado){
    var acierto=false;
    if(cuadradosAbiertos[posX][posY]==-1){
      tusPuntos--;
      cuadradosAbiertos[posX][posY]=0;
    }
    else if(cuadradosAbiertos[posX][posY]==1){
      cuadradosAbiertos[posX][posY]=2;
      tusPuntos=tusPuntos+15;
      acierto=true;
      sonidoAcierto.pause();
      sonidoAcierto.currentTime = 0;
      sonidoAcierto.play();
    }
    dibujar();
    if(!acierto){
      sonidoFallo.pause();
      sonidoFallo.currentTime = 0;
      sonidoFallo.play();
      cambiarTurno();
    }else{
      if(comprobarHaPerdidoJugador(false)){
        puntos1++;
        document.getElementById("lResultadoRes").innerHTML="HAS GANADO CON "+tusPuntos+" PUNTOS :D resultado "+puntos1+"-"+puntos2;
        if(puntos2>=cuantasPartidas||puntos1>=cuantasPartidas){
           document.getElementById("lResultadoRes").innerHTML+=" FIN";
        }
        sonidoWin.pause();
        sonidoWin.currentTime = 0;
        sonidoWin.play();
        $('#modalFinal').modal('show');
      }
    }

  }else{
    if(escanerActivado){
      lanzarEscaner(posX,posY);
    }else if(bombaActivada){
      lanzarBomba(posX,posY);
    }
  }

}
function lanzarEscaner(){
  sonidoEscaner.pause();
  sonidoEscaner.currentTime = 0;
  sonidoEscaner.play();

  escanerActivado=false;
  var cuantos=0;
  for(var i=-1;i<=1;i++){
    for(var j=-1;j<=1;j++){
      //alert((cuadradoSelecX+i)+" "+(cuadradoSelecY+j));
      if(cuadradoSelecX+i>=0&&cuadradoSelecY+j>=0&&cuadradoSelecX+i<cuadradosAncho&&cuadradoSelecY+j<cuadradosAlto){
        if(cuadradosAbiertos[cuadradoSelecX+i][cuadradoSelecY+j]==1||cuadradosAbiertos[cuadradoSelecX+i][cuadradoSelecY+j]==2){
          cuantos++;
        }
        //cuadradosAbiertos[cuadradoSelecX+i][cuadradoSelecY+j]=2;
      }
    }
  }
  numEscaners--;
  cambiarTurno();
  dibujar();
  actualizarArmas();
  //alert("El escaner detecta "+cuantos+" espacios ocupados");
  document.getElementById("lEscanerRes").innerHTML="El escaner a detectado "+cuantos+" bloques ocupados";
  $('#modalEscaner').modal('show');
}
function cerrarResultadoEscaner(){
  $('#modalEscaner').modal('hide');
}
function lanzarBomba(){
  sonidoBigExplosion.pause();
  sonidoBigExplosion.currentTime = 0;
  sonidoBigExplosion.play();

  bombaEnXY(cuadradoSelecX,cuadradoSelecY);
  bombaEnXY(cuadradoSelecX-1,cuadradoSelecY);
  bombaEnXY(cuadradoSelecX+1,cuadradoSelecY);
  bombaEnXY(cuadradoSelecX,cuadradoSelecY-1);
  bombaEnXY(cuadradoSelecX,cuadradoSelecY+1);

  bombaActivada=false;
  numBombas--;
  cambiarTurno();
  dibujar();
  actualizarArmas();
}
function bombaEnXY(posX,posY){
  if(cuadradosAbiertos[posX][posY]==-1){
      cuadradosAbiertos[posX][posY]=0;
      tusPuntos--;
    }
    else if(cuadradosAbiertos[posX][posY]==1){
      cuadradosAbiertos[posX][posY]=2;
      tusPuntos=tusPuntos+15;

      acierto=true;
    }
}
function disparoEnemigo(posX,posY){
  var acierto=false;
  if(cuadradosAbiertosTuyos[posX][posY]==-1){
    cuadradosAbiertosTuyos[posX][posY]=0;
    tusPuntos++;
  }
  else if(cuadradosAbiertosTuyos[posX][posY]==1){
    cuadradosAbiertosTuyos[posX][posY]=2;
    acierto=true;
    tusPuntos=tusPuntos-5;
  }
  dibujar();
  if(!acierto){
    cambiarTurno();
  }else{
    if(comprobarHaPerdidoJugador(true)){
      puntos2++;
      document.getElementById("lResultadoRes").innerHTML="HAS PERDIDO :( resultado "+puntos1+"-"+puntos2;
        if(puntos2>=cuantasPartidas||puntos1>=cuantasPartidas){
           document.getElementById("lResultadoRes").innerHTML+=" FIN";
        }
        sonidoLose.pause();
        sonidoLose.currentTime = 0;
        sonidoLose.play();
        $('#modalFinal').modal('show');
    }else{
      setTimeout(function() { turnoCOntrario(); }, 500+Math.random()*500);
    }
  }
}
function getMousePos(canvas, evt) {
        var rect = canvas.getBoundingClientRect();
        return {
          x: evt.clientX - rect.left,
          y: evt.clientY - rect.top
        };
      }
function dibujar(){
  var c = document.getElementById("myCanvas");
  var ctx = c.getContext("2d");
  ctx.fillStyle="white";
  ctx.rect(0,0,cuadradosAncho*tamCuadrados*2+100,cuadradosAlto*tamCuadrados);
  ctx.fill();
  for(var i=0;i<cuadradosAbiertos.length;i++){
    var linea=cuadradosAbiertos[i];
    for(var j=0;j<linea.length;j++){
      var ideCuadrado=linea[j];
     
        if(ideCuadrado==-1||ideCuadrado==1){
          ctx.fillStyle="#000000";
          ctx.fillRect(i*tamCuadrados,j*tamCuadrados,tamCuadrados,tamCuadrados);
          
        } else if(ideCuadrado==0){
          ctx.drawImage(agua,i*tamCuadrados,j*tamCuadrados);
        }
        else if(ideCuadrado==1){
          ctx.drawImage(agua,i*tamCuadrados,j*tamCuadrados);
          ctx.drawImage(barco,i*tamCuadrados,j*tamCuadrados);
        }
        else if(ideCuadrado==2){
          ctx.drawImage(agua,i*tamCuadrados,j*tamCuadrados);
          ctx.drawImage(barco,i*tamCuadrados,j*tamCuadrados);
          ctx.drawImage(disparoAgua,i*tamCuadrados,j*tamCuadrados);
        }
         if(cuadradoSelecX==i&&cuadradoSelecY==j&&esMiTurno&&!escanerActivado&&!bombaActivada){
          ctx.drawImage(cuadradoVerde,i*tamCuadrados,j*tamCuadrados);
        }
      
    }
  }
  if(cuadradoSelecX>=0&&cuadradoSelecX<cuadradosAncho&&cuadradoSelecY>=0&&cuadradoSelecY<cuadradosAlto){
    if(escanerActivado){
      ctx.drawImage(imgEscaner,cuadradoSelecX*tamCuadrados,cuadradoSelecY*tamCuadrados);
      ctx.drawImage(cuadradoEscaner,(cuadradoSelecX-1)*tamCuadrados,cuadradoSelecY*tamCuadrados);
      ctx.drawImage(cuadradoEscaner,cuadradoSelecX*tamCuadrados,(cuadradoSelecY-1)*tamCuadrados);
      if(cuadradoSelecX<cuadradosAncho-1){
        if(cuadradoSelecY<cuadradosAlto-1){
          ctx.drawImage(cuadradoEscaner,(cuadradoSelecX+1)*tamCuadrados,(cuadradoSelecY+1)*tamCuadrados);
        }
        if(cuadradoSelecY>=1){
          ctx.drawImage(cuadradoEscaner,(cuadradoSelecX+1)*tamCuadrados,(cuadradoSelecY-1)*tamCuadrados);
        }

        ctx.drawImage(cuadradoEscaner,(cuadradoSelecX+1)*tamCuadrados,cuadradoSelecY*tamCuadrados);
      }
      if(cuadradoSelecY<cuadradosAlto-1){
        if(cuadradoSelecX>=1){
          ctx.drawImage(cuadradoEscaner,(cuadradoSelecX-1)*tamCuadrados,(cuadradoSelecY+1)*tamCuadrados);
        }
        
        ctx.drawImage(cuadradoEscaner,cuadradoSelecX*tamCuadrados,(cuadradoSelecY+1)*tamCuadrados);
      }
       ctx.drawImage(cuadradoEscaner,(cuadradoSelecX-1)*tamCuadrados,(cuadradoSelecY-1)*tamCuadrados);
        
    }
    if(bombaActivada){
      ctx.drawImage(imgBomba,cuadradoSelecX*tamCuadrados,cuadradoSelecY*tamCuadrados);
      ctx.drawImage(cuadradoBomba,(cuadradoSelecX-1)*tamCuadrados,cuadradoSelecY*tamCuadrados);
      if(cuadradoSelecX<cuadradosAncho-1){
        ctx.drawImage(cuadradoBomba,(cuadradoSelecX+1)*tamCuadrados,cuadradoSelecY*tamCuadrados);
      }
      ctx.drawImage(cuadradoBomba,cuadradoSelecX*tamCuadrados,(cuadradoSelecY-1)*tamCuadrados);
      if(cuadradoSelecY<cuadradosAlto-1){
        ctx.drawImage(cuadradoBomba,cuadradoSelecX*tamCuadrados,(cuadradoSelecY+1)*tamCuadrados);
      }
    }
  }

  var movTuyos=tamCuadrados*cuadradosAncho+100;
  for(var i=0;i<cuadradosAbiertosTuyos.length;i++){
    var linea=cuadradosAbiertosTuyos[i];
    for(var j=0;j<linea.length;j++){
      var ideCuadrado=linea[j];
     
        if(ideCuadrado==-1){//-1 agua sin ver /0 agua +disparo /1barco /2barco +disparo
          ctx.drawImage(agua,i*tamCuadrados+movTuyos,j*tamCuadrados);
        }else if(ideCuadrado==0){
          ctx.drawImage(agua,i*tamCuadrados+movTuyos,j*tamCuadrados);
          ctx.drawImage(disparoAgua,i*tamCuadrados+movTuyos,j*tamCuadrados);
        }else if(ideCuadrado==1){
          ctx.drawImage(agua,i*tamCuadrados+movTuyos,j*tamCuadrados);
          ctx.drawImage(barco,i*tamCuadrados+movTuyos,j*tamCuadrados);
        }else if(ideCuadrado==2){
          ctx.drawImage(agua,i*tamCuadrados+movTuyos,j*tamCuadrados);
          ctx.drawImage(barco,i*tamCuadrados+movTuyos,j*tamCuadrados);
          ctx.drawImage(disparoAgua,i*tamCuadrados+movTuyos,j*tamCuadrados);
        }
        
      
    }
  }

  if(colocandoBarcos){
    if(entraBarco(true,tamanioBarcoColocarAct,cuadradoSelecX,cuadradoSelecY,barcoColocandoHorizontal)&cuadradoSelecX>=0){
      for(var i=0;i<tamanioBarcoColocarAct;i++){
        if(barcoColocandoHorizontal){
          ctx.drawImage(barco,(cuadradoSelecX+i)*tamCuadrados+movTuyos,cuadradoSelecY*tamCuadrados);
        }else{
          ctx.drawImage(barco,cuadradoSelecX*tamCuadrados+movTuyos,(cuadradoSelecY+i)*tamCuadrados);
        }
      }
    }else{
      if(cuadradoSelecX>=0&&cuadradoSelecX<cuadradosAncho&&cuadradoSelecY>=0&&cuadradoSelecY<cuadradosAlto){
        ctx.drawImage(cuadradoRojo,cuadradoSelecX*tamCuadrados+movTuyos,cuadradoSelecY*tamCuadrados);
      }
    }
  }

  ctx.font="18px Georgia";
  ctx.fillText("Tus Puntos",cuadradosAncho*tamCuadrados+4,cuadradosAlto*tamCuadrados/2-20);
  ctx.fillText(tusPuntos,cuadradosAncho*tamCuadrados+4+40,cuadradosAlto*tamCuadrados/2+10);

}
function iniciarTusBarcos(){
  /*cuadradosAbiertosTuyos[0][4]=1;
  cuadradosAbiertosTuyos[1][4]=1;
  cuadradosAbiertosTuyos[2][4]=1;

  cuadradosAbiertosTuyos[7][2]=1;
  cuadradosAbiertosTuyos[7][1]=1;
  cuadradosAbiertosTuyos[7][3]=1;

  cuadradosAbiertosTuyos[4][7]=1;
  cuadradosAbiertosTuyos[4][8]=1;
  cuadradosAbiertosTuyos[4][9]=1;

  cuadradosAbiertosTuyos[12][8]=1;
  cuadradosAbiertosTuyos[12][7]=1;

  cuadradosAbiertosTuyos[8][6]=1;
  cuadradosAbiertosTuyos[8][7]=1;

  cuadradosAbiertosTuyos[7][10]=1;
  cuadradosAbiertosTuyos[8][10]=1;

  cuadradosAbiertosTuyos[11][4]=1;

  cuadradosAbiertosTuyos[3][8]=1;*/
  for(var i=numeroBarcos.length-1;i>=0;i--){
    var tamanio=i+1;
    var cuantosBarcos=numeroBarcos[i];
    //console.log("Colocando "+cuantosBarcos+" de tamano "+tamanio);

    for(var j=0;j<cuantosBarcos;j++){
      var rotacionNum=Math.random();
      var horizontal=rotacionNum>0.5;
      var posX;
      var posY;
      if(horizontal){
        posX=Math.round(Math.random()*(cuadradosAncho-tamanio-1));
        posY=Math.round(Math.random()*(cuadradosAlto-1));
      }else{
        posY=Math.round(Math.random()*(cuadradosAlto-tamanio-1));
        posX=Math.round(Math.random()*(cuadradosAncho-1));
      }
      while(entraBarco(true,tamanio,posX,posY,horizontal)==false){
        if(horizontal){
        posX=Math.round(Math.random()*(cuadradosAncho-tamanio-1));
        posY=Math.round(Math.random()*(cuadradosAlto-1));
        }else{
          posY=Math.round(Math.random()*(cuadradosAlto-tamanio-1));
          posX=Math.round(Math.random()*(cuadradosAncho-1));
        }
      }
      colocarBarco(true,tamanio,posX,posY,horizontal);
      
    }
  }
}

function iniciarSusBarcos(){
  /*cuadradosAbiertos[0][4]=1;
  cuadradosAbiertos[1][4]=1;
  cuadradosAbiertos[2][4]=1;

  cuadradosAbiertos[7][2]=1;
  cuadradosAbiertos[7][1]=1;
  cuadradosAbiertos[7][3]=1;

  cuadradosAbiertos[4][7]=1;
  cuadradosAbiertos[4][8]=1;
  cuadradosAbiertos[4][9]=1;

  cuadradosAbiertos[12][8]=1;
  cuadradosAbiertos[12][7]=1;

  cuadradosAbiertos[8][6]=1;
  cuadradosAbiertos[8][7]=1;

  cuadradosAbiertos[7][10]=1;
  cuadradosAbiertos[8][10]=1;

  cuadradosAbiertos[11][4]=1;

  cuadradosAbiertos[3][8]=1;*/
  for(var i=numeroBarcos.length-1;i>=0;i--){
    var tamanio=i+1;
    var cuantosBarcos=numeroBarcos[i];
    //console.log("Colocando "+cuantosBarcos+" de tamano "+tamanio);

    for(var j=0;j<cuantosBarcos;j++){
      var rotacionNum=Math.random();
      var horizontal=rotacionNum>0.5;
      var posX;
      var posY;
      if(horizontal){
        posX=Math.round(Math.random()*(cuadradosAncho-tamanio-1));
        posY=Math.round(Math.random()*(cuadradosAlto-1));
      }else{
        posY=Math.round(Math.random()*(cuadradosAlto-tamanio-1));
        posX=Math.round(Math.random()*(cuadradosAncho-1));
      }
      while(entraBarco(false,tamanio,posX,posY,horizontal)==false){
        if(horizontal){
        posX=Math.round(Math.random()*(cuadradosAncho-tamanio-1));
        posY=Math.round(Math.random()*(cuadradosAlto-1));
        }else{
          posY=Math.round(Math.random()*(cuadradosAlto-tamanio-1));
          posX=Math.round(Math.random()*(cuadradosAncho-1));
        }
      }
      colocarBarco(false,tamanio,posX,posY,horizontal);
      
    }
  }
}


function cambiarTurno(){
  esMiTurno=!esMiTurno;
  if(esMiTurno){
    document.getElementById("lTurno").style.display="inline";
    document.getElementById("lTurnoContrario").style.display="none";
  }else{
    document.getElementById("lTurno").style.display="none";
    document.getElementById("lTurnoContrario").style.display="inline";
    setTimeout(function() { turnoCOntrario(); }, 500+Math.random()*500);
    
  }
}
function turnoCOntrario(){
  var posX=Math.round(Math.random()*(cuadradosAncho-1));
  var posY=Math.round(Math.random()*(cuadradosAlto-1));

  while(cuadradosAbiertosTuyos[posX][posY]==0||cuadradosAbiertosTuyos[posX][posY]==2){
      var posX=Math.round(Math.random()*(cuadradosAncho-1));
      var posY=Math.round(Math.random()*(cuadradosAlto-1));
  }
  var trampas=0;
  trampas=dificultad*1;
  var cont=0;
  while(cuadradosAbiertosTuyos[posX][posY]==0||cuadradosAbiertosTuyos[posX][posY]==2||cuadradosAbiertosTuyos[posX][posY]==-1){
      if(cont>=trampas){
        break;
      }
      var posX=Math.round(Math.random()*(cuadradosAncho-1));
      var posY=Math.round(Math.random()*(cuadradosAlto-1));
      cont++;
  }


  disparoEnemigo(posX,posY);
}
function comprobarHaPerdidoJugador(local){
  var algunBarco=false;
  var matriz;
  if(local){
    matriz=cuadradosAbiertosTuyos;
  }else{
    matriz=cuadradosAbiertos;
  }

   for(var i=0;i<matriz.length;i++){
    var linea=matriz[i];
    for(var j=0;j<linea.length;j++){
      var ideCuadrado=linea[j];
      if(ideCuadrado==1){
        algunBarco=true;
        break;
      }
    }
   }
   return !algunBarco;

}

function entraBarco(local,tamanoBarco,posX,posY,enHorizontal){
  try {
    var matriz;
    if(local){
      matriz=cuadradosAbiertosTuyos;
    }else{
      matriz=cuadradosAbiertos;
    }

    if(enHorizontal){
      if(posX+tamanoBarco>cuadradosAncho){
        return false;
      }
      if(posY>=cuadradosAlto){
        return false;
      }
      /*for(var i=0;i<tamanoBarco;i++){
        if(matriz[posX+i][posY]==1){
          return false;
        }
      }*/
      for(var i=-1;i<tamanoBarco+1;i++){
        for(var j=-1;j<2;j++){
          var xAct=posX+i;
          var yAct=posY+j;
          if(xAct>=0&&xAct<cuadradosAncho&&yAct>=0&&yAct<cuadradosAlto){
            if(matriz[xAct][yAct]==1){
              return false;
            }
          }
        }
        
      }
      return true;
    }else{
      if(posY+tamanoBarco>cuadradosAlto){
        return false;
      }
      if(posX>=cuadradosAncho){
        return false;
      }
      /*for(var i=0;i<tamanoBarco;i++){
        if(matriz[posX][posY+i]==1){
          return false;
        }
      }*/
      for(var i=-1;i<tamanoBarco+1;i++){
        for(var j=-1;j<2;j++){
          var xAct=posX+j;
          var yAct=posY+i;
          if(xAct>=0&&xAct<cuadradosAncho&&yAct>=0&&yAct<cuadradosAlto){
            if(matriz[xAct][yAct]==1){
              return false;
            }
          }
        }
        
      }
      return true;
    }
  }catch(e){
    return false;
  }

}
function colocarBarco(local,tamanoBarco,posX,posY,enHorizontal){
  //console.log("Colocando de tamanio "+tamanoBarco+" en "+posX+" "+posY+" rotacion "+enHorizontal);
  if(local){
    matriz=cuadradosAbiertosTuyos;
  }else{
    matriz=cuadradosAbiertos;
  }
  for(var i=0;i<tamanoBarco;i++){
    if(enHorizontal){
      matriz[posX+i][posY]=1;
    }else{
      matriz[posX][posY+i]=1;
    }
  }
}

function avanzarEnColocarBarcos(){
  numeroBarcosPorColocarTamanioAct--;
  if(numeroBarcosPorColocarTamanioAct==0){
    tamanioBarcoColocarAct--;
    if(tamanioBarcoColocarAct==0){
      if(contraJUgador){
        colocandoBarcos=false;
        esperarBarcosRivales();
        comprobarSiRivalHaColocadoBarcos();
        return 0;
      }else{
        iniciarJuego();
      }
    }else{
      numeroBarcosPorColocarTamanioAct=numeroBarcos[tamanioBarcoColocarAct-1];
    }

  }
  if(colocandoBarcos){
    barcosFaltanColocar--;
    actualizarLabelColocandoBarcos();
    dibujar();

  }
  
}
function esperarBarcosRivales(){

  document.getElementById("avisoColocar").innerHTML="Esperando barcos del rival...";
  document.getElementById("colocandoActual").innerHTML="Ten paciencia ;)";

}
function comprobarSiRivalHaColocadoBarcos(){

}
function actualizarLabelColocandoBarcos(){
  document.getElementById("colocandoActual").innerHTML="Quedan "+barcosFaltanColocar+" barcos (pulsa R para rotar)";


}
function actualizarArmas(){
  document.getElementById("lBomba").innerHTML="x"+numBombas+" Bombas";
  document.getElementById("lEscaner").innerHTML="x"+numEscaners+" Escaners";
}
function iniciarJuego(){
  colocandoBarcos=false;
  esMiTurno=true;
  document.getElementById("divColocarBarcos").style.display="none";
  document.getElementById("divTurnos").style.display="block";
}
function getQueryVariable(variable)
{
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}
function clickBomba(){
  escanerActivado=false;
  if(bombaActivada){
    bombaActivada=false;
  }else{
    if(numBombas>0){
      bombaActivada=true;
    }
  }
}
function clickEscaner(){
  bombaActivada=false;
  if(escanerActivado){
    escanerActivado=false;
  }else if(numEscaners>0){
    escanerActivado=true;
  }

}
function volverMenu(){
  document.location.href = "index.html";
}
function volverJugar(){
  if(puntos2>=cuantasPartidas||puntos1>=cuantasPartidas){
     document.location.href = "partida.html?tam="+getQueryVariable("tam")+"&nivel="+getQueryVariable("nivel")+"&barcos1="+getQueryVariable("barcos1")+"&barcos2="+getQueryVariable("barcos2")+"&barcos3="+getQueryVariable("barcos3")+"&barcos4="+getQueryVariable("barcos4")+"&bombas="+getQueryVariable("bombas")+"&escaners="+getQueryVariable("escaners")+"&numPartidas="+getQueryVariable("numPartidas")+"&puntos1=0&puntos2=0";
  
  }else{
    document.location.href = "partida.html?tam="+getQueryVariable("tam")+"&nivel="+getQueryVariable("nivel")+"&barcos1="+getQueryVariable("barcos1")+"&barcos2="+getQueryVariable("barcos2")+"&barcos3="+getQueryVariable("barcos3")+"&barcos4="+getQueryVariable("barcos4")+"&bombas="+getQueryVariable("bombas")+"&escaners="+getQueryVariable("escaners")+"&numPartidas="+getQueryVariable("numPartidas")+"&puntos1="+puntos1+"&puntos2="+puntos2;
  }
}