# Hundir la Euskal

Juego de batalla naval basado en HTML5 + Javascript, utilizando jQuery y Bootstrap para mejorar el aspecto visual de la aplicación. Desarrollado para el concurso de FOSS Coding de la Euskal Encounter 22.

## Entorno de desarrollo

- Sublime text 2

## ¿Cómo hacerlo funcionar?

Descomprimir fichero y abrir index.html con algún navegador actual (lo hemos probado con Firefox en Ubuntu 14.10 y Chrome en Windows) y listo. Recomendamos usar Chrome.

##Objetivos cumplidos

### Básico:

- Juego para un sólo jugador contra la máquina.
- Sistema de turnos para gestionar las acciones de “elección de coordenadas” hasta que el jugador gane o abandone la partida.
- Feedback del resultado del envío del torpedo al jugador que lo lanzó. Aplicación del resultado en la rejilla del jugador atacado.


### Extras

- Diferentes niveles de dificultad vs ordenador.
- Opciones de configuración: tamaño de rejilla y tipo y número de barcos configurables.
- “Magias especiales”: Bombas de mayor alcance y escaner que indica el número de casillas ocupadas.
- No sólo reflejar el ganador sino la puntuación obtenida (elige la fórmula que más te guste).
- Efectos de sonido de disparos, armas especiales


### Extras a los que no hemos llegado:

- Soporte multijugador. A pesar de tener la parte de servidor hecha, no hemos podido conectarlo por problemas de diseño y de tiempo.

-----------------------

*Toda la música e imagenes del juego tienen una licencia libre*.

**Grupo SINFONIER**: Ander (a.k.a. Goteck) Aranbarri [BL-114] y Gorka (a.k.a. Gollumiko) Urkiola [BL-113].

_Euskal Encounter 21
26/07/2014_